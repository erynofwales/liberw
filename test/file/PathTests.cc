/* PathTests.cc
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * Tests for path manipulation functions.
 */

#include "gtest/gtest.h"

#include "file/Path.hh"

using namespace ::erw;

#pragma mark - erw::path::join

TEST(PathJoin, EmptyStringGivesEmptyString)
{
    EXPECT_EQ("", path::join(""));
}

TEST(PathJoin, RootFirstGivesSingleLeadingSlash)
{
    EXPECT_EQ("/abc", path::join("/", "abc"));
}

TEST(PathJoin, MultipleSlashesPreservesAll)
{
    EXPECT_EQ("abc///def", path::join("abc///", "def"));
}

#pragma mark - erw::path::split

TEST(PathSplit, EmptyPathGivesEmptyTuple)
{
    std::tuple<erw::String, erw::String> expected("", "");
    EXPECT_EQ(expected, path::split(""));
}

TEST(PathSplit, RootGivesRootAndEmpty)
{
    std::tuple<erw::String, erw::String> expected("/", "");
    EXPECT_EQ(expected, path::split("/"));
}

TEST(PathSplit, SingleComponentGivesSingleComponentAndEmpty)
{
    std::tuple<erw::String, erw::String> exp1("abc", "");
    EXPECT_EQ(exp1, path::split("abc"));

    std::tuple<erw::String, erw::String> exp2("/abc", "");
    EXPECT_EQ(exp2, path::split("/abc"));
}

TEST(PathSplit, TwoComponentsGiveFirstAndLast)
{
    std::tuple<erw::String, erw::String> exp1("abc", "def");
    EXPECT_EQ(exp1, path::split("abc/def"));

    std::tuple<erw::String, erw::String> exp2("/abc", "def");
    EXPECT_EQ(exp2, path::split("/abc/def"));
}
