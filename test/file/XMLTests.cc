/* XMLTests.cc
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * Testing the XML facilities.
 */

#include "gtest/gtest.h"

#include "file/XMLParser.hh"

using namespace ::erw;

#pragma mark - erw::XMLDocument

TEST(XML, EmptyStringGivesEmptyDocument)
{
    auto doc = XMLDocument::parseString("");
    EXPECT_EQ(doc, nullptr);
}

class XMLSimpleDocument
    : public ::testing::Test
{
    virtual void
    SetUp()
    {
        doc = XMLDocument::parseString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                       "<note>"
                                       "    <to rel=\"self\">Eryn</to>"
                                       "  <from rel=\"mother\">Mom</from>"
                                       "<heading>Reminder</heading>"
                                       " <body>Don't forget the milk!</body>"
                                       " <img src=\"birthdaycake.jpg\"/>"
                                       "</note>");
    }

protected:
    XMLDocument::UCPtr doc;

    void
    checkNode(XMLNode::WCPtr node,
              String name,
              String content)
        const
    {
        auto nodePtr = node.lock();
        ASSERT_TRUE(bool(nodePtr));
        EXPECT_EQ(name, nodePtr->name());
        EXPECT_EQ(content, nodePtr->content());
    }

    XMLNode::CPtr
    root()
        const
    {
        return node(doc->root());
    }

    XMLNode::CPtr
    node(XMLNode::WCPtr weakNode)
        const
    {
        return weakNode.lock();
    }
};


TEST_F(XMLSimpleDocument, IsValid)
{
    ASSERT_NE(nullptr, doc);
    ASSERT_TRUE(bool(root()));
}


TEST_F(XMLSimpleDocument, HasARootNodeWith4Children)
{
    auto rootPtr = root();
    EXPECT_EQ("note", rootPtr->name());
    EXPECT_EQ(5UL, rootPtr->children().size());
}


TEST_F(XMLSimpleDocument, ChildrenAreCorrectlyParsed)
{
    XMLNode::WCList children = root()->children();
    ASSERT_EQ(5UL, children.size());
    checkNode(children[0], "to", "Eryn");
    checkNode(children[1], "from", "Mom");
    checkNode(children[2], "heading", "Reminder");
    checkNode(children[3], "body", "Don't forget the milk!");
    checkNode(children[4], "img", "");
}


TEST_F(XMLSimpleDocument, ChildAttributesAreCorrectlyParsed)
{
    auto children = root()->children();
    ASSERT_EQ(5UL, children.size());
    EXPECT_EQ("self", children[0].lock()->attributes().at("rel"));
    EXPECT_EQ("mother", children[1].lock()->attributes().at("rel"));
    EXPECT_EQ("birthdaycake.jpg", children[4].lock()->attributes().at("src"));
}
