/* PosixFile.hh
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * File description.
 */

#include "File.hh"
#include "String.hh"


namespace erw {

struct PosixFile
    : public File
{
    PosixFile(const String& path, File::Mode mode);

    virtual ~PosixFile();

    virtual ssize_t read(char* buffer, size_t bytes = SIZE_MAX) override;
    virtual ssize_t write(char* buffer, size_t bytes) override;

    virtual void seek(size_t pos) override;
    virtual void seek(ssize_t offset, File::SeekFrom from) override;

protected:
    int mFileDescriptor;

    virtual void close() override;

    int modeToFlags(File::Mode mode) const noexcept;
    int seekFromToWhence(File::SeekFrom from) const noexcept;
};

} /* namespace erw */
