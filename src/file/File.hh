/* File.hh
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */

#pragma once

#include <memory>

#include "String.hh"


namespace erw {

struct File
{
    typedef std::unique_ptr<File> UPtr;
    typedef std::shared_ptr<File> Ptr;

    enum class SeekFrom {
        /** Seek from the beginning of the file. */
        Begin,
        /** Seek from the current file offset. */
        Here,
        /** Seek from the end of the file. */
        End,
    };

    /** Open a file for reading. */
    static UPtr openForReading(const String& path);

    /** Open a file for writing. */
    static UPtr openForWriting(const String& path);

    /**
     * Open a file for updating. Writes will be appended to the end of the
     * file.
     */
    static UPtr openForUpdating(const String& path);

    /** Deleted copy constructor. Copying open files is not allowed. */
    File(const File& other) = delete;

    /** Destructor. Subclasses must close the file in their own sweet way. */
    virtual ~File();

    const String& path() const;

    /**
     * @defgroup I/O
     * @{
     */

    /**
     * Read `bytes` bytes into `buffer`. If `bytes == SIZE_MAX`, read all
     * available data into the buffer.
     */
    virtual ssize_t read(char* buffer, size_t bytes = SIZE_MAX) = 0;

    /** Write `bytes` bytes of the contents of `buffer` into the open file. */
    virtual ssize_t write(char* buffer, size_t bytes) = 0;

    /** @} */

    /** Seek to an absolute position in the file. */
    virtual void seek(size_t pos) = 0;

    /** Seek to an offset from the given point in the file. */
    virtual void seek(ssize_t offset, SeekFrom from) = 0;

protected:
    enum Mode {
        Read = 1,
        Write = 2,
        Update = 4,
    };

    String mPath;

    /** Default constructor. */
    File(const String& path);

    /** Close the underlying file. */
    virtual void close() = 0;
};

} /* namespace erw */
