/* Path.cc
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * Path manipulation utilities.
 */

#include "Path.hh"


namespace erw {
namespace path {

#if defined(_WIN32) || defined(_WIN64)
const String Separator = "\\";
#else
const String Separator = "/";
#endif


String
basename(const String& path)
{
    return std::get<1>(split(path));
}


String
dirname(const String& path)
{
    return std::get<0>(split(path));
}


std::tuple<String, String>
split(const String& path)
{
    auto lastSlash = path.rfind(Separator);
    if (lastSlash == 0 || lastSlash == String::npos) {
        return { path, "" };
    }
    return { path.substr(0, lastSlash), path.substr(++lastSlash) };
}

} /* namespace path */
} /* namespace erw */
