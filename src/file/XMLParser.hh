/* XMLParser.hh
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * An XML Parser.
 */

#pragma once

#include <map>
#include <memory>
#include <vector>

#include "File.hh"
#include "String.hh"


namespace erw {

struct XMLDocument;

/** A node in the XML tree. */
struct XMLNode
{
    typedef std::shared_ptr<XMLNode> Ptr;
    typedef std::shared_ptr<const XMLNode> CPtr;
    typedef std::weak_ptr<XMLNode> WPtr;
    typedef std::weak_ptr<const XMLNode> WCPtr;
    typedef std::vector<Ptr> List;
    typedef std::vector<WCPtr> WCList;
    typedef std::map<String, String> AttributeMap;

    virtual ~XMLNode();

    virtual String name() const noexcept = 0;
    virtual String content() const noexcept = 0;
    virtual WCList children() const noexcept;
    virtual AttributeMap attributes() const noexcept = 0;

protected:
    /** Children of this node. The node owns its children. */
    List mChildren;

    XMLNode(List&& children);
};


/** An XML document. */
struct XMLDocument
{
    typedef std::unique_ptr<const XMLDocument> UCPtr;

    /**
     * Parse an open file into an XMLDocument. The returned document is
     * read-only.
     *
     * @param [in] file     The file to parse.
     * @return A pointer to a parsed document, or nullptr.
     */
    static UCPtr parseFile(File::UPtr file);

    static UCPtr parseString(const String& string);

    virtual ~XMLDocument();

    /**
     * Get the root node of the document. The root node is returned as a weak
     * ptr. When the document is destroyed, the root and all its descendents
     * will be freed. It is up to anything holding a reference to this object to
     * check that it is still valid before dereferencing.
     *
     * @return The root node.
     */
    virtual XMLNode::WCPtr root() const noexcept = 0;

protected:
    /** The root of the XML tree. The document owns its root. */
    XMLNode::Ptr mRoot;

    XMLDocument(XMLNode::Ptr root);
};

} /* namespace erw */
