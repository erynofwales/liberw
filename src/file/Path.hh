/* Path.hh
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * Path utilities.
 */

#pragma once

#include <tuple>
#include "String.hh"


namespace erw {
namespace path {

/**
 * System-defined path separator. "/" for Unix systems, "\" for Windows
 * systems.
 */
extern const String Separator;


/**
 * Return the the last component of the given path.
 *
 * @param [in]  The path.
 * @return      The base name of the path.
 */
String basename(const String& path);


/**
 * Return all but the last component of the given path.
 *
 * @param [in]  The path.
 * @return      The dir name of the path.
 */
String dirname(const String& path);


/**
 * Path join base case.
 *
 * @return The empty string.
 * @see join()
 */
inline String
join()
{
    return "";
}


/**
 * Path-aware join. Joins the provided arguments to produce a string
 * representing a path, using `Separator` between components.
 *
 * @param [in] first    First component of the path.
 * @param [in] parts    Subsequent components of the path.
 * @return              The joined path.
 */
template<typename... Args>
inline String
join(String first, Args... parts)
{
    String last = join(parts...);
    if (first.empty()) {
        return last;
    }
    String out = first;
    String firstSeparator = first.substr(first.size() - Separator.size());
    if (firstSeparator != Separator && last.size() > 0) {
        out += Separator;
    }
    if (last.size() > 0) {
        out += last;
    }
    return out;
}


/**
 * Split the path into a (head, tail) pair.
 *
 * @param [in] path     The path to split.
 * @return The split path.
 */
std::tuple<String, String> split(const String& path);

} /* namespace path */
} /* namespace erw */
