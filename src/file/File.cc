/* File.cc
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */

#include "File.hh"
#include "private/PosixFile.hh"


namespace erw {

/* static */ File::UPtr
File::openForReading(const String& path)
{
    return UPtr(new PosixFile(path, Mode::Read));
}


/* static */ File::UPtr
File::openForWriting(const String& path)
{
    return UPtr(new PosixFile(path, Mode::Write));
}


/* static */ File::UPtr
File::openForUpdating(const String& path)
{
    return UPtr(new PosixFile(path, Mode::Update));
}


File::File(const String& path)
    : mPath(path)
{ }


File::~File()
{ }


const String&
File::path()
    const
{
    return mPath;
}

} /* namespace erw */

