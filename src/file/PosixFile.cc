/* PosixFile.cc
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * Implementation of PosixFile.
 */

#include <cassert>
#include <fcntl.h>

#include "private/PosixFile.hh"


namespace erw {

PosixFile::PosixFile(const String& path,
                     File::Mode mode)
    : File(path),
      mFileDescriptor(::open(path.c_str(), modeToFlags(mode)))
{
    // TODO: Check for errors with fd == -1.
}


PosixFile::~PosixFile()
{
    close();
}


ssize_t
PosixFile::read(char* buffer,
                size_t bytes)
{
    // TODO: Errors!
    return ::read(mFileDescriptor, buffer, bytes);
}


ssize_t
PosixFile::write(char* buffer,
                 size_t bytes)
{
    // TODO: Errors!
    return ::write(mFileDescriptor, buffer, bytes);
}


void
PosixFile::seek(size_t pos)
{
    ::lseek(mFileDescriptor, pos, SEEK_SET);
}


void
PosixFile::seek(ssize_t offset,
                File::SeekFrom from)
{
    ::lseek(mFileDescriptor, offset, seekFromToWhence(from));
}


void
PosixFile::close()
{
    if (mFileDescriptor != -1) {
        ::close(mFileDescriptor);
        mFileDescriptor = -1;
    }
}


int
PosixFile::modeToFlags(File::Mode mode)
    const noexcept
{
    switch (mode) {
        case File::Mode::Read:
            return O_RDONLY;
        case File::Mode::Write:
            return O_WRONLY;
        case File::Mode::Update:
            return O_WRONLY | O_APPEND;
        default:
            assert(false);
            return 0;
    }
}


int
PosixFile::seekFromToWhence(File::SeekFrom from)
    const noexcept
{
    switch (from) {
        case File::SeekFrom::Begin:
            return SEEK_SET;
        case File::SeekFrom::Here:
            return SEEK_CUR;
        case File::SeekFrom::End:
            return SEEK_END;
        default:
            assert(false);
            return 0;
    }
}

} /* namespace erw */
