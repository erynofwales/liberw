/* bits.hh
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * Utilities for working with bits.
 */

#pragma once


namespace erw {

template<typename T>
inline T
swapBytes(const T value)
    noexcept
{
    constexpr size_t nbytes = sizeof(T);

    union {
        T v;
        uint8_t bytes[nbytes];
    } a, b;

    a.v = value;
    for (int i = 0; i < (nbytes - i - 1); i++) {
        b.bytes[i] = a.bytes[nbytes - i - 1];
    }
    return b.v;
}

} /* namespace erw */
