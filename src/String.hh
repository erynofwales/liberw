/* String.hh
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * Basic string class.
 */

#include <string>


namespace erw {

typedef std::string String;

} /* namespace erw */
